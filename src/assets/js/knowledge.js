// console.log('page knowledge');

import swiper from '../../../public/vendor/swiper/swiper.min.js';
import '../../../public/vendor/swiper/swiper.min.css';
export default {
	data() {
		return {
			canNext1: false,
			canNext2: false,
			canNext4: false,
			canNext6: false,
			canNext8: false,
			effectConfig: {
				rotate: 0,
	            stretch: 0,
	            depth: 100,
	            modifier: 2,
	            slideShadows: false
			},
			index1: 0,
			index2: 0,
			index4: 0,
			index6: 0,
			index8: 0,
			an4_1: true, 
			an4_2: true, 
			an6_1: true, 
			an6_2: true, 
			an8_1: true, 
			an8_2: true
		};
	},
	created() {
		this.$root.checkStatus();
		if(this.$root.unlock) {
			this.canNext1 = true;
			this.canNext2 = true;
			this.canNext4 = true;
			this.canNext6 = true;
			this.canNext8 = true;
		};
	},
	mounted() {
		let that = this;
		that.$nextTick(() => {
			that.ksallSwiper =  new Swiper('.box-all .ksall', {
				direction : 'vertical',
				resistanceRatio: 0,
				onInit: (swiper) => {
					if(!that.$root.unlock) {
						swiper.lockSwipeToNext();
					};
				},
				onSlideChangeStart: function(swiper) {
					switch(swiper.realIndex) {
						case 0:
							if(that.canNext1) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 1:
							if(that.canNext2) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 2:
							swiper.unlockSwipeToNext();
							break;
						case 3:
							if(that.an4_1) {
								that.an4_1 = false;
								$('.text4-1-1').addClass('an1');
								$('.text4-1-2').addClass('an2');
								$('.text4-1-3').addClass('an3');
							};
							if(that.canNext4) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 4:
							swiper.unlockSwipeToNext();
							break;
						case 5:
							if(that.an6_1) {
								that.an6_1 = false;
								$('.text6-1-1').addClass('an1');
								$('.text6-1-2').addClass('an2');
								$('.text6-1-3').addClass('an3');
							};
							if(that.canNext6) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 6:
							swiper.unlockSwipeToNext();
							break;
						case 7:
							if(that.an8_1) {
								that.an8_1 = false;
								$('.text8-1-1').addClass('an1');
								$('.text8-1-2').addClass('an2');
								$('.text8-1-3').addClass('an3');
							};
							if(that.canNext8) {
								swiper.unlockSwipeToNext();
							} else {
								swiper.lockSwipeToNext();
							};
							break;
						case 8:
							swiper.unlockSwipeToNext();
							break;
						default: //
					};
				}
			});
			that.ksSwiper1 =  new Swiper('.box-swiper1 .ks1', {
				initialSlide: that.index1,
				effect: 'coverflow',
				coverflow: that.effectConfig,
				onSlideChangeStart: (swiper) => {
					that.index1 = swiper.realIndex;
				},
				onReachEnd: (swiper) => {
					that.canNext1 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper2 =  new Swiper('.box-swiper2 .ks2', {
				initialSlide: that.index2,
				effect: 'coverflow',
				coverflow: that.effectConfig,
				onSlideChangeStart: (swiper) => {
					that.index2 = swiper.realIndex;
				},
				onReachEnd: (swiper) => {
					that.canNext2 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper4 =  new Swiper('.box-swiper4 .ks4', {
				initialSlide: that.index4,
				effect: 'coverflow',
				coverflow: that.effectConfig,
				onSlideChangeStart: (swiper) => {
					that.index4 = swiper.realIndex;
					if(swiper.realIndex == 1) {
						if(that.an4_2) {
							that.an4_2 = false;
							$('.text4-2-1').addClass('an1');
							$('.text4-2-2').addClass('an2');
							$('.text4-2-3').addClass('an3');
						};
					};
				},
				onReachEnd: (swiper) => {
					that.canNext4 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper6 =  new Swiper('.box-swiper6 .ks6', {
				initialSlide: that.index6,
				effect: 'coverflow',
				coverflow: that.effectConfig,
				onSlideChangeStart: (swiper) => {
					that.index6 = swiper.realIndex;
					if(swiper.realIndex == 1) {
						if(that.an6_2) {
							that.an6_2 = false;
							$('.text6-2-1').addClass('an1');
							$('.text6-2-2').addClass('an2');
							$('.text6-2-3').addClass('an3');
							$('.text6-2-4').addClass('an4');
							$('.text6-2-5').addClass('an5');
						};
					};
				},
				onReachEnd: (swiper) => {
					that.canNext6 = true;
					that.ksallSwiper.unlockSwipeToNext();
				}
			});
			that.ksSwiper8 =  new Swiper('.box-swiper8 .ks8', {
				initialSlide: that.index8,
				effect: 'coverflow',
				coverflow: that.effectConfig,
				onSlideChangeStart: (swiper) => {
					that.index8 = swiper.realIndex;
					if(swiper.realIndex == 1) {
						if(that.an8_2) {
							that.an8_2 = false;
							$('.text8-2-1').addClass('an1');
							$('.text8-2-2').addClass('an2');
						};
					};
				},
				onReachEnd: (swiper) => {
					that.canNext8 = true;
					that.ksallSwiper.unlockSwipeToNext();
					that.setCompleted();
				}
			});
		});
	},
	methods: {
		// home
		toHome() {
			this.$router.replace('/home');
		},
		// to Prev
		toPrev() {
			this.ksallSwiper.slidePrev();
		},
		// to next
		toNext() {
			this.ksallSwiper.slideNext();
		},
		// to Prev
		toprev(n) {
			switch(n) {
				case 1:
					this.ksSwiper1.slidePrev();
					break;
				case 2:
					this.ksSwiper2.slidePrev();
					break;
				case 4:
					this.ksSwiper4.slidePrev();
					break;
				case 6:
					this.ksSwiper6.slidePrev();
					break;
				case 8:
					this.ksSwiper8.slidePrev();
					break;
				default: //
			};
		},
		// to next
		tonext(n) {
			switch(n) {
				case 1:
					this.ksSwiper1.slideNext();
					break;
				case 2:
					this.ksSwiper2.slideNext();
					break;
				case 4:
					this.ksSwiper4.slideNext();
					break;
				case 6:
					this.ksSwiper6.slideNext();
					break;
				case 8:
					this.ksSwiper8.slideNext();
					break;
				default: //
			};
		},
		// complete
		setCompleted() {
			if(!this.$root.unlock) {
				this.$root.unlock = true;
            	sessionStorage.setItem('status', JSON.stringify(this.$root.unlock));
			};
			let postData = {lesson_status: 'completed'};
			top.postMessage(JSON.stringify(postData), '*');
		}
	}
};
