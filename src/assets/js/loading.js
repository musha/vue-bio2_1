// console.log('page loading');

import jQuery from '../../../public/vendor/loader/jquery-1.8.3.min.js' // html5loader
import html5Loader from '../../../public/vendor/loader/jquery.html5Loader.min.js' // html5loader
export default {
	data() {
		return {
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
        let that = this,
    		// 预加载
            firstLoadFiles = {
                'files': [
                    // common
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-top.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/green.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/logo.png'),
                        'size': 8192
                    },
                    // home
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/banner.jpg'),
                        'size': 60416
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/bottle.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/btn-inter.png'),
                        'size': 7168
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/title.png'),
                        'size': 14336
                    },
                    // knowledge
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd-a.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd-intro.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd-learn.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd-q.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowd-rule.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowl.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowr.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/arrowu.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/banner.png'),
                        'size': 179200
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/bg-banner.png'),
                        'size': 11264
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/bg-bottom1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/bg-bottom2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/bg-bottom3.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/btn-home.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/molecule1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/molecule2.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-1.png'),
                        'size': 9216
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-2.png'),
                        'size': 36864
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-3.png'),
                        'size': 33792
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide1-4.png'),
                        'size': 39936
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-1.png'),
                        'size': 10240
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-2.png'),
                        'size': 27648
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-3.png'),
                        'size': 30720
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide2-4.png'),
                        'size': 30720
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide3.png'),
                        'size': 12288
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide4-1.jpg'),
                        'size': 27648
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide4-2.jpg'),
                        'size': 37888
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide5.png'),
                        'size': 14336
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide6-1.jpg'),
                        'size': 25600
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide6-2.jpg'),
                        'size': 26624
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide7.png'),
                        'size': 14336
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide8-1.jpg'),
                        'size': 40960
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide8-2.jpg'),
                        'size': 28672
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/slide9.png'),
                        'size': 34816
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-1-1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-1-2.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-1-3.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-2-1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-2-2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text4-2-3.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-1-1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-1-2.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-1-3.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-2-1.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-2-2.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-2-3.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-2-4.png'),
                        'size': 1024
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text6-2-5.png'),
                        'size': 9216
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text8-1-1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text8-1-2.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text8-1-3.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text8-2-1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/knowledge/text8-2-2.png'),
                        'size': 6144
                    }
                ]
            };
        $.html5Loader({
            filesToLoad: firstLoadFiles,
            onBeforeLoad: function() {},
            onElementLoaded: function(obj, elm) {},
            onUpdate: function(percentage) {
                // console.log(percentage);
                $('.box-progress .bar').animate({width: (percentage + '%')}, 10, () => {
                    $('.percent').html(percentage);
                    if(percentage == 100) {
                        setTimeout(() => {
                            that.$root.checkParams();
                        }, 1000);
                    };
                });
            },
            onComplete: function() {}
        });
	},
	methods: {}
};
