import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vendor
import ydui from '../public/vendor/ydui.flexible.js'; // 页面分辨率

// import Vconsole from 'vconsole'; // vconsole
// const vConsole = new Vconsole();
// export default vConsole;

Vue.config.devtools = false;
Vue.config.productionTip = false;

new Vue({
  	router,
  	render: h => h(App),
  	data() {
  		return {
  			info: {},
            unlock: false
  		}
  	},
    created() {},
  	methods: {
        // get param
        getParameterByName(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
                results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        // is set
        isset(param) {
            if(param != '' && param != null && param != undefined) {
                return true;
            } else {
                return false;
            };
        },
  		// check params
  		checkParams() {
            if(this.isset(this.getParameterByName('userId')) && this.isset(this.getParameterByName('rname')) && this.isset(this.getParameterByName('courseId')) && this.isset(this.getParameterByName('completed'))) {
                let userId = this.getParameterByName('userId'),
                    rname = this.getParameterByName('rname'),
                    courseId = this.getParameterByName('courseId'),
                    completed = this.getParameterByName('completed');
                if(completed == 1) {
                    this.unlock = true;
                    sessionStorage.setItem('status', JSON.stringify(this.unlock));
                };
            };
            this.$router.replace('home');
		},
		// check unlock status
		checkStatus() {
			if(this.isset(JSON.parse(sessionStorage.getItem('status')))) {
				this.unlock = JSON.parse(sessionStorage.getItem('status'));
			};
		}
  	}
}).$mount('#app')
